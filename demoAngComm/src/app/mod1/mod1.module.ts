import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Comp1Component } from './comp1/comp1.component';
import { Mod12Module } from './mod12/mod12.module';
import { Comp12Component } from './mod12/comp12/comp12.component';



@NgModule({
  declarations: [Comp1Component],
  imports: [
    CommonModule, Mod12Module
  ],
  exports: [
    Comp1Component, Comp12Component
  ]
})
export class Mod1Module { }
